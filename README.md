# ReSiSa

Resisa is a [bovine](https://codeberg.org/bovine/bovine/) based RSS to ActivityPub bridge. Usage has two parts

```bash
python -mresisa.add_feed host url
```

adds the RSS feed at url to be collected and posted to host. You will then be displayed a did-key that you have to add to the appropriate actor on the host.

By running

```bash
poetry run python -mresisa.update_feeds
```

you can trigger an update for the RSS feeds. I recommend adding this to crontab.

## Managing Topics

Run

```bash
poetry run python -mresisa.topics --topic TOPIC_NAME
```

to add a new topic page

## Todo

- [ ] Delete old content
- [ ] Update content
- [ ] Better filtering
- [ ] Describe the topic mechanism
