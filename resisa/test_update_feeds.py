import bovine

from .update_feeds import build_note_from_feed_entry


def test_build_note_from_feed_entry():
    client = bovine.BovineClient(
        domain="my_host",
        secret="my_private_key",
    )

    client.information = {"id": "actor_id", "followers": "followers"}

    entry = {
        "id": "https://orf.at/stories/3314214/",
        "title": "Nieder\u00f6sterreich beschlie\u00dft Kindergartennovelle",
        "title_detail": {
            "type": "text/plain",
            "language": None,
            "base": "https://rss.orf.at/news.xml",
            "value": "Nieder\u00f6sterreich beschlie\u00dft Kindergartennovelle",
        },
        "links": [
            {
                "rel": "alternate",
                "type": "text/html",
                "href": "https://orf.at/stories/3314214/",
            }
        ],
        "link": "https://orf.at/stories/3314214/",
        "tags": [{"term": "Inland", "scheme": None, "label": None}],
        "updated": "2023-04-27T18:51:50+02:00",
        "updated_parsed": [2023, 4, 27, 16, 51, 50, 3, 117, 0],
        "orfon_usid": "news:3314214",
        "orfon_oewacategory": {
            "rdf:resource": "urn:oewa:RedCont:Politik/PolitikInland"
        },
        "orfon_storytype": {"rdf:resource": "urn:orfon:type:ticker"},
    }

    result = build_note_from_feed_entry(client, entry)

    result["source"]["content"] = "XXX"

    assert "published" in result
    del result["published"]

    assert result == {
        "@context": "https://www.w3.org/ns/activitystreams",
        "attributedTo": "actor_id",
        "cc": ["followers"],
        "content": "-<p>Inland</p>",
        "inReplyTo": "https://mymath.rocks/objects/b5106ff6-dafa-40b4-9b5a-192552d06c04",
        "source": {
            "content": "XXX",
            "mediaType": "application/json",
        },
        "summary": "Niederösterreich beschließt Kindergartennovelle",
        "to": ["https://www.w3.org/ns/activitystreams#Public"],
        "type": "Note",
        "url": "https://orf.at/stories/3314214/",
    }
