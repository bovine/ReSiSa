async def create_to_outbox_and_return_object_id(client, obj):
    create = client.activity_factory.create(obj).build()
    response = await client.send_to_outbox(create)
    activity_location = response.headers["location"]

    activity = await client.proxy(activity_location)

    return activity["object"]["id"]
