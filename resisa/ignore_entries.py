def should_ignore(entry):
    title = entry.get("title", "-")

    if title.startswith("heise+"):
        return True

    if title.startswith("heise-Angebot"):
        return True

    return False
