import asyncio
import json

from datetime import datetime, timedelta, timezone

from dateutil.parser import parse

import bovine
import feedparser

from .database import Database
from .models import FeedSource, FeedEntry
from .utils import create_to_outbox_and_return_object_id
from .assign_topic_page import topic_page_for_tag
from .ignore_entries import should_ignore


def build_note_from_feed_entry(client, entry):
    feed_summary = entry.get("title", "-")
    feed_content = entry.get("summary", "-")
    feed_url = entry.get("link")

    source = {"content": json.dumps(entry, indent=2), "mediaType": "application/json"}
    topic_page = None

    tags = entry.get("tags")
    if tags and len(tags) > 0:
        for tag in tags:
            if topic_page:
                break
            topic_page = topic_page_for_tag(tag["term"])

        tags = [t["term"] for t in tags]
        feed_content += "<p>" + ", ".join(tags) + "</p>"

    note = (
        client.object_factory.note(
            content=feed_content,
            summary=feed_summary,
            url=feed_url,
            source=source,
            in_reply_to=topic_page,
            published=bovine.utils.now_isoformat(),
        )
        .as_public()
        .build()
    )

    return note


async def send_create_entry_to_outbox(client, entry, db_entry):
    note = build_note_from_feed_entry(client, entry)

    object_id = await create_to_outbox_and_return_object_id(client, note)

    db_entry.remote_object_id = object_id
    await db_entry.save()


async def remove_unneeded_objects(object_ids):
    for feed_id in object_ids:
        await FeedEntry.filter(feed_id=feed_id).delete()


async def handle_feed(feed):
    async with bovine.BovineClient(domain=feed.host, secret=feed.secret) as client:
        raw_feed = feedparser.parse(feed.url)

        feed_ids_in_db = await FeedEntry.filter(feed_source=feed).all()
        feed_ids_in_db = {x.feed_id for x in feed_ids_in_db}
        entries_to_update = set()

        for entry in raw_feed["entries"]:
            feed_url = entry.get("link")
            feed_published = entry.get("published")

            feed_updated = entry.get("updated", feed_published)
            feed_id = entry.get("id", feed_url)
            if feed_id is None:
                print("CANNOT HANDLE")
                print(json.dumps(entry, indent=2))
                continue

            if feed_id in feed_ids_in_db:
                feed_ids_in_db.remove(feed_id)

            feed_updated = parse(feed_updated)

            if feed_updated < datetime.now(timezone.utc) - timedelta(days=2):
                continue

            if should_ignore(entry):
                continue

            db_entry, created = await FeedEntry.get_or_create(
                feed_source=feed,
                feed_id=feed_id,
                defaults={"updated": feed_updated, "remote_object_id": None},
            )

            if db_entry.remote_object_id is None:
                await send_create_entry_to_outbox(client, entry, db_entry)

            if db_entry.updated != feed_updated:
                entries_to_update.add((entry, db_entry))
                db_entry.updated = feed_updated
                await db_entry.save()

                print(feed_id, " should be updated")

        await remove_unneeded_objects(feed_ids_in_db)


async def update_feeds():
    async with Database():
        feeds = await FeedSource.filter().all()

        for feed in feeds:
            await handle_feed(feed)


def main():
    asyncio.run(update_feeds())


if __name__ == "__main__":
    main()
