import asyncio
from argparse import ArgumentParser

from bovine.crypto import generate_ed25519_private_key, private_key_to_did_key
import bovine

from .database import Database
from .utils import create_to_outbox_and_return_object_id


async def create_topic(host, secret, topic):
    async with bovine.BovineClient(domain=host, secret=secret) as client:
        page = client.object_factory.note(content=topic).as_public().build()
        page["type"] = "Page"

        page_id = await create_to_outbox_and_return_object_id(client, page)

        print(f"Created topic {topic} with page_id {page_id}")


async def manage(args):
    async with Database() as database:
        if args.host:
            await database.set("actor_host", args.host)

            secret = generate_ed25519_private_key()

            await database.set("actor_secret", secret)

            did_key = private_key_to_did_key(secret)

            print(f"Please ad the did key {did_key} to the corresponding actor")

            return

        if args.topic:
            host = await database.get("actor_host")
            secret = await database.get("actor_secret")

            await create_topic(host, secret, args.topic)


def main():
    parser = ArgumentParser("management of topics")

    parser.add_argument("--host", help="Sets the hostname")
    parser.add_argument("--topic", help="Adds a new topic")

    args = parser.parse_args()

    asyncio.run(manage(args))


if __name__ == "__main__":
    main()
