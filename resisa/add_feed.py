import asyncio
from argparse import ArgumentParser

import feedparser
import bovine

from .models import FeedSource
from .database import Database


def build_parser():
    parser = ArgumentParser("Add a feed to resisa")
    parser.add_argument("host", help="ActivityPub Actor Host")
    parser.add_argument("url", help="url of the feed")

    return parser


async def register_feed(host, private_key, feed_url, feed_title):
    async with Database():
        await FeedSource.create(
            host=host, secret=private_key, title=feed_title, url=feed_url
        )


def main():
    parser = build_parser()
    args = parser.parse_args()

    host = args.host

    feed = feedparser.parse(args.url)
    feed_url = args.url
    feed_title = feed["feed"]["title"]

    private_key = bovine.crypto.generate_ed25519_private_key()
    did_key = bovine.crypto.private_key_to_did_key(private_key)

    print(f"Please add did-key {did_key} to your actor on {host}")

    asyncio.run(register_feed(host, private_key, feed_url, feed_title))


if __name__ == "__main__":
    main()
