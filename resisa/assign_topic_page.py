def topic_page_for_tag(tag):
    topics = dict(
        Politik="https://mymath.rocks/objects/8e709711-6e8c-49e1-bb32-023e45c302a4",
        Kultur="https://mymath.rocks/objects/424d1cdd-c382-4307-bdfb-49655c1a12e0",
        Culture="https://mymath.rocks/objects/424d1cdd-c382-4307-bdfb-49655c1a12e0",
        Media="https://mymath.rocks/objects/424d1cdd-c382-4307-bdfb-49655c1a12e0",
        Sport="https://mymath.rocks/objects/7df773a8-b27d-406d-99f3-a94bda4cf25b",
        Chronik="https://mymath.rocks/objects/b3256ed2-a0f6-43ee-b986-07167f2d1afd",
        Inland="https://mymath.rocks/objects/b5106ff6-dafa-40b4-9b5a-192552d06c04",
        Ausland="https://mymath.rocks/objects/024b831b-ece4-41e6-b2e4-a9ba0b7606d9",
        Wirtschaft="https://mymath.rocks/objects/179ed440-8bc1-4992-8c22-7542c3a3f5c3",
        Business="https://mymath.rocks/objects/179ed440-8bc1-4992-8c22-7542c3a3f5c3",
        Umwelt="https://mymath.rocks/objects/45046830-67b9-4311-941e-98c6a69de168",
        Panorama="https://mymath.rocks/objects/62b2df8f-8e42-4e5b-b385-61c32d61e93c",
        Reise="https://mymath.rocks/objects/62b2df8f-8e42-4e5b-b385-61c32d61e93c",
        Travel="https://mymath.rocks/objects/62b2df8f-8e42-4e5b-b385-61c32d61e93c",
        Leute="https://mymath.rocks/objects/62b2df8f-8e42-4e5b-b385-61c32d61e93c",
        Religion="https://mymath.rocks/objects/62b2df8f-8e42-4e5b-b385-61c32d61e93c",
        Science="https://mymath.rocks/objects/eedd5783-f1d5-452e-a3f4-9ec99a9340b9",
    )
    topics["US news"] = (
        "https://mymath.rocks/objects/024b831b-ece4-41e6-b2e4-a9ba0b7606d9",
    )

    if tag in topics:
        return topics[tag]
