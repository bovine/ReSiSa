import os

from tortoise import Tortoise
from .models import ConfigurationVariable


class Database:
    def __init__(self, db_file="resisa.sqlite3"):
        self.db_file = db_file
        self.db_url = f"sqlite://{db_file}"

    async def __aenter__(self):
        db_exists = os.path.exists(self.db_file)

        await Tortoise.init(db_url=self.db_url, modules={"models": ["resisa.models"]})

        if not db_exists:
            await Tortoise.generate_schemas()

        return self

    async def __aexit__(self, exc_type, exc_value, trace):
        await Tortoise.close_connections()

    async def get(self, key: str) -> str | None:
        var = await ConfigurationVariable.get_or_none(key=key)

        if not var:
            return None

        return var.value

    async def set(self, key: str, value: str):
        await ConfigurationVariable.update_or_create(key=key, defaults={"value": value})
