import asyncio
from .database import Database
from .models import FeedSource, FeedEntry
import bovine


async def cleanup_feed(feed):
    entries = await FeedEntry.filter(feed_source=feed).all()
    remote_objects = [x.remote_object_id for x in entries]

    async with bovine.BovineClient(domain=feed.host, secret=feed.secret) as client:
        outbox = await client.outbox()
        async for item in outbox.iterate(max_number=100):
            obj = item["object"]
            if item["type"] == "Create":
                if isinstance(obj, dict):
                    obj = obj["id"]

                if obj not in remote_objects:
                    print(obj)


async def cleanup_outboxes():
    async with Database():
        feeds = await FeedSource.filter().all()

        feed = feeds[0]
        await cleanup_feed(feed)


def main():
    asyncio.run(cleanup_outboxes())


if __name__ == "__main__":
    main()
