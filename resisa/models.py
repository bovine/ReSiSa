from tortoise import fields
from tortoise.models import Model


class FeedSource(Model):
    id = fields.IntField(pk=True)

    url = fields.CharField(max_length=255)
    title = fields.CharField(max_length=255)

    host = fields.CharField(max_length=255)
    secret = fields.CharField(max_length=255)


class FeedEntry(Model):
    id = fields.IntField(pk=True)

    feed_source = fields.ForeignKeyField("models.FeedSource")

    feed_id = fields.CharField(max_length=255)
    remote_object_id = fields.CharField(max_length=255, null=True)
    updated = fields.DatetimeField(auto_now_add=True)


class ConfigurationVariable(Model):
    id = fields.IntField(pk=True)

    key = fields.CharField(max_length=255)
    value = fields.CharField(max_length=255)
