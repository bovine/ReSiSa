import os

from .database import Database


async def test_database():
    db_file = "test_db.sqlite3"
    async with Database(db_file=db_file):
        pass

    assert os.path.exists(db_file)

    os.unlink(db_file)


async def test_config_vars():
    db_file = "test_db.sqlite3"
    async with Database(db_file=db_file) as database:
        result = await database.get("key")
        assert result is None

        await database.set("key", "value")

        result = await database.get("key")
        assert result == "value"

    os.unlink(db_file)
